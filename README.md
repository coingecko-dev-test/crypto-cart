# Cryptocurrency Store with Shopping Cart
Assume a fictitious world where CoinGecko is a brokerage/dealer for 
cryptocurrencies such as Bitcoin, Ethereum, and others.

Create a mobile app using React Native that allows user to search and browse
for coins listed on [CoinGecko](https://www.coingecko.com/en). 

Users can purchase any cryptocurencies at market price with a shopping cart.

### Requirements:
- Users can search for a coin based on name (eg. bitcoin) or symbols (eg. BTC) and browse from a list of coins - displayed with current market price
- Add a coin with amount to a shopping cart
- Shopping cart should allow them to edit line item amount and calculate total based on current market price
- If the user quits the app. They should be able to return and continue adding to the existing shopping cart.
- Once the user is satisfied, they can "checkout" at market price
- Users can view past orders in a history section
- A simple and neat UI, layout should be responsive to portrait and landscape
- Please write test cases against the functions implemented.
- **Bonus point: As cryptocurrency users care about privacy, encrypt the data and make the app accessible with an unlock pin/password

*Note: For the shopping cart "checkout" step, you dont need to worry about how payment is being made, once it's clicked it should be created as a history/order*

## Part 1: Planning the deliveries
Describe how you would plan the project and a high level design of the architecture. 
What principles or design practices would you consider incorporating into the process and technology? 
You can include diagrams. You do not need to write any code for this.

## Part 2: Implementation
The app should be able to allow users to search for coins, add them to shopping cart, and finally checkout according to market value.

### Stack
- React Native (Mandatory)
- [CoinGecko API docs](https://www.coingecko.com/en/api) for cryptocurrency data
- You are free to choose any approach to solve the problem above. Approaches will be discussed via a pull request or during on-site interview.

## Submitting your solution
- Use Git version control to commit your changes. (You may create a Gitlab account, create a free private repository, and add us as collaborators)
- We recommend creating a pull request highlighting relevant changes for ease of discussion
- The code should compile into an executable as an APK (or on iOS)
- Provide instructions for setup and launch. (It’s usually a README file)

### Code commits
- If you are using a framework and it has boilerplate code generation I would suggest creating the initial commit to separate the boilerplate code from yours
- Commit often as this would help convey your thought process virtually

## Getting Started (Fork this code)
- Fork this code into your Gitlab account as a "Private" repository and invite the users in https://gitlab.com/groups/coingecko-dev-test/-/group_members
- Branch out of the master branch and create a Merge Request. We can go through the code and discuss there

